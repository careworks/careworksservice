package com.foreforgood.careworks.controller;


import com.foreforgood.careworks.data.SchoolProfileDataRepo;
import com.foreforgood.careworks.model.SchoolProfile;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foreforgood.careworks.data.RoleDataRepo;
import com.foreforgood.careworks.data.SchoolProfileDataRepo;
import com.foreforgood.careworks.data.UserDataRepository;
import com.foreforgood.careworks.model.SchoolProfile;
import com.foreforgood.careworks.security.JwtTokenProvider;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/auth")
public class SchoolProfileController {

    @Autowired
    SchoolProfileDataRepo schoolrepo;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDataRepository userRepository;

    @Autowired
    RoleDataRepo roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @GetMapping("/getSchoolProfile")
    public SchoolProfile authenticateUser(@RequestParam String name) {
        return schoolrepo.findByName(name);
    }

    @GetMapping("/getSchoolProfile/all")
    public List<SchoolProfile> authenticateUser() {
        return schoolrepo.findAll();
    }


    @PostMapping("/addSchoolProfile")
    public SchoolProfile addSchoolProfile(@Valid @RequestBody SchoolProfile schoolProfile) {
        return  schoolrepo.save(schoolProfile);
    }


}