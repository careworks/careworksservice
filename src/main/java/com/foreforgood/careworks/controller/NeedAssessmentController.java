package com.foreforgood.careworks.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foreforgood.careworks.data.AssessmentQuestionDataRepo;
import com.foreforgood.careworks.model.AssessmentQuestion;

@RestController
@RequestMapping("/api/auth")
public class NeedAssessmentController {
	
	@Autowired
	AssessmentQuestionDataRepo assessmentrepo;
	
	@GetMapping("/getAssessmentQuestions")
    public List<AssessmentQuestion> authenticateUser(@RequestParam String section) {
        return assessmentrepo.findBySection(section);
    }

    @GetMapping("/getAssessmentQuestions/all")
    public List<AssessmentQuestion>  authenticateUser() {
        return assessmentrepo.findAll();
    }

}
