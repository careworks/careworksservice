package com.foreforgood.careworks.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "assessment_option")
public class AssessmentOption {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int optionId;
	private String optionName;	
	
	public int getOptionId() {
		return optionId;
	}
	public void setOptionId(int optionId) {
		this.optionId = optionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	@Override
	public String toString() {
		return "AssessmentOption [optionId=" + optionId + ", optionName=" + optionName + "]";
	}
	
	
	
	
	
	
}
