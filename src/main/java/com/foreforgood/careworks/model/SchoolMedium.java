package com.foreforgood.careworks.model;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.DateTimeException;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "school_medium")
public class SchoolMedium {

    @Id
    private String medium;


    public SchoolMedium() {
    }

    public SchoolMedium(String medium) {
        this.medium = medium;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String type) {
        this.medium = medium;
    }


}
