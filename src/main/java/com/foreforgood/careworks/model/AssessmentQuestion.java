package com.foreforgood.careworks.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "assessment_question")
public class AssessmentQuestion {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int questionId;
	private String question;
	private String section;
	private String questionType;
	
	@ManyToMany
	@JoinTable(name="question_option",
	joinColumns= @JoinColumn(name="question_id"),
	inverseJoinColumns= @JoinColumn(name="option_id"))
	private Set<AssessmentOption> options;
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Set<AssessmentOption> getOptions() {
		return options;
	}
	public void setOptions(Set<AssessmentOption> options) {
		this.options = options;
	}
	
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	@Override
	public String toString() {
		return "AssessmentQuestion [questionId=" + questionId + ", question=" + question + ", section=" + section
				+ ", questionType=" + questionType + ", options=" + options + "]";
	}
	
	
	
	
	
	
	

}
