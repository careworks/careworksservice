package com.foreforgood.careworks.model;

import javax.persistence.*;

@Entity
@Table(name = "school_category")
public class SchoolCategory {

    @Id
    private String category;

    private String description;

    public SchoolCategory() {
    }

    public SchoolCategory(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
