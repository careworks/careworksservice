package com.foreforgood.careworks.model;

public enum RoleName {
	ROLE_USER,
    ROLE_ADMIN
}
