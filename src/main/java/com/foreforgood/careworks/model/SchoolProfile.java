package com.foreforgood.careworks.model;



import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.DateTimeException;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "school_profile")
public class SchoolProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long school_id;

    private String name;
    private String address;
    private String schoolCluster;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "medium")
    private SchoolMedium medium;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="category")
    private SchoolCategory category;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="type")
    private SchoolType type;

    private String emailId;
    private String diseCode;
    private String block;
    private String region;
    private String district;
    private String state;
    private int studentCount;
    private int establishedYear;
    private String location;



    public SchoolProfile() {

    }

    public SchoolProfile(String name, String address, String schoolCluster, SchoolCategory category, SchoolType type, String emailId, String diseCode, String block, String region, String district, String state, int studentCount, int establishedYear) {
        this.name = name;
        this.address = address;
        this.schoolCluster = schoolCluster;
        this.category = category;
        this.type = type;
        this.emailId = emailId;
        this.diseCode = diseCode;
        this.block = block;
        this.region = region;
        this.district = district;
        this.state = state;
        this.studentCount = studentCount;
        this.establishedYear = establishedYear;
    }

    public Long getSchool_id() {
        return school_id;
    }

    public void setSchool_id(Long school_id) {
        this.school_id = school_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchoolCluster() {
        return schoolCluster;
    }

    public void setSchoolCluster(String schoolCluster) {
        this.schoolCluster = schoolCluster;
    }

    public SchoolCategory getCategory() {
        return category;
    }

    public void setCategory(SchoolCategory category) {
        this.category = category;
    }

    public SchoolType getType() {
        return type;
    }

    public void setType(SchoolType type) {
        this.type = type;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getDiseCode() {
        return diseCode;
    }

    public void setDiseCode(String diseCode) {
        this.diseCode = diseCode;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }


    public String getLocation() {
        return location;
    }

    public int getEstablishedYear() {
        return establishedYear;
    }

    public void setEstablishedYear(int establishedYear) {
        this.establishedYear = establishedYear;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
