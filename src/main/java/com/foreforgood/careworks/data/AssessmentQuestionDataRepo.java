package com.foreforgood.careworks.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foreforgood.careworks.model.AssessmentQuestion;

public interface AssessmentQuestionDataRepo extends JpaRepository<AssessmentQuestion, Integer> {
	
	List<AssessmentQuestion> findBySection(String section);
	List<AssessmentQuestion> findAll();
	

}
