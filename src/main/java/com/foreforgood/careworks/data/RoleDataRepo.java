package com.foreforgood.careworks.data;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.foreforgood.careworks.model.Role;
import com.foreforgood.careworks.model.RoleName;

public interface RoleDataRepo extends JpaRepository<Role, Integer> {
	Optional<Role> findByName(RoleName roleName);
}

