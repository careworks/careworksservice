package com.foreforgood.careworks.data;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foreforgood.careworks.model.SchoolProfile;


public interface SchoolProfileDataRepo extends JpaRepository<SchoolProfile, String> {
    SchoolProfile findByName(String name);
    List<SchoolProfile> findAll();

}

