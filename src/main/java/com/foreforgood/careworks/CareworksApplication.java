package com.foreforgood.careworks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CareworksApplication {

	public static void main(String[] args) {
		SpringApplication.run(CareworksApplication.class, args);
	}

}

