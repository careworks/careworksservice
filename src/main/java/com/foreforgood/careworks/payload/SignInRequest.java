package com.foreforgood.careworks.payload;
import javax.validation.constraints.*;

public class SignInRequest {

    @NotBlank
    @Size(max = 40)
    private String userNameOrEmail;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;


    public String getuserNameOrEmail() {
        return userNameOrEmail;
    }

    public void setuserNameOrEmail(String userNameOrEmail) {
        this.userNameOrEmail = userNameOrEmail;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
